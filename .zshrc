#
# ~/.zshrc
#

typeset -U PATH path
path=("${HOME}/.local/share/npm-global/bin"
      "${HOME}/.config/composer/vendor/bin" 
			"${HOME}/.scripts" "$path[@]")
export PATH

fpath=("${HOME}/.config/zsh/functions" "$fpath[@]")

# The following lines are only for interactive shells
[[ $- = *i* ]] || return

if [[ -r ~/.aliasrc ]]; then
	source ~/.aliasrc
fi

# history setup
HISTSIZE=1000
SAVEHIST=$HISTSIZE
HISTFILE="${XDG_CACHE_HOME:-$HOME/.cache}/zsh/history"
setopt INC_APPEND_HISTORY    # Immediately append commands to history file.
setopt SHARE_HISTORY				 # Share the same history between instances.
setopt HIST_IGNORE_ALL_DUPS  # Never add duplicate entries.
setopt HIST_IGNORE_SPACE     # Ignore commands that start with a space.
setopt HIST_REDUCE_BLANKS    # Remove unnecessary blank lines.

# Do not exit on end-of-file. Require the use of exit or logout  instead.
setopt IGNORE_EOF

# The output is a shell command to set the LS_COLORS environment variable.
test -r ~/.dir_colors && eval $(dircolors ~/.dir_colors)

# load custom key bindings
source ${HOME}/.config/zsh/key-bindings.zsh

# optional fzf key bindings (and completion)
# Ctrl+t list files+folders in current directory
# Ctrl+r search history of shell commands
# Alt+c fuzzy change directory
source /usr/share/fzf/key-bindings.zsh
source /usr/share/fzf/completion.zsh

# Edit the current command line in $EDITOR
autoload -U edit-command-line
zle -N edit-command-line
bindkey '^v' edit-command-line

# initialize completion system
source ${HOME}/.config/zsh/completion.zsh

# z.lua init with enhanced mode
eval "$(lua $HOME/.scripts/z.lua --init zsh enhanced once)"

autoload -Uz colors && colors

autoload -Uz nnncd
bindkey -s '^n' 'nnncd\n'
bindkey -s '^f' 'cd "$(dirname "$(fzf)")"\n'

dictl () 
{
	dict "$@" | less
}

# plugins
source ${HOME}/.config/zsh/plugins/git/git-prompt.sh
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh

# git prompt options
# unstaged (*) and staged (+) changes
GIT_PS1_SHOWDIRTYSTATE=true
# If something is stashed, then a '$' will be shown.
GIT_PS1_SHOWSTASHSTATE=true
# If there're untracked files, then a '%' will be shown.
GIT_PS1_SHOWUNTRACKEDFILES=true
# A "<" indicates you are behind, ">" indicates you are ahead, 
# "<>" indicates you have diverged and "=" indicates that there is no difference.
GIT_PS1_SHOWUPSTREAM="auto"
GIT_PS1_STATESEPARATOR=' '
GIT_PS1_HIDE_IF_PWD_IGNORED=true
GIT_PS1_COMPRESSSPARSESTATE=true

# user prompt
setopt PROMPT_SUBST

PS1='%(?.%F{green}.%F{red})%T> %F{blue}%n> %F{magenta}%3~$(__git_ps1 " (%s)")%(!.#.>)%f '
