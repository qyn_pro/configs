return {
  -- the colorscheme should be available when starting Neovim
  {
    'arcticicestudio/nord-vim', -- An arctic, north-bluish clean and elegant Vim theme
    lazy = false, -- make sure we load this during startup if it is your main colorscheme
    priority = 1000, -- make sure to load this before all the other start plugins
    config = function()
      require 'plugins.configs.nordvim'
      -- load the colorscheme here
      vim.cmd.colorscheme 'nord'
    end,
  },

  { -- set statusline
    'itchyny/lightline.vim',
    config = function()
      require 'plugins.configs.lightline'
    end,
  },

  'tpope/vim-unimpaired', -- pairs of handy bracket mappings
  'tpope/vim-surround', -- surrounding text objects with parenthesis, quotes, tags
  'tpope/vim-repeat', -- enable repeating supported plugin maps with "."
  'nelstrom/vim-visual-star-search', -- start a * or # search from a visual block
  'tpope/vim-obsession', -- continuously updated session files

  -- supports line (//) and block (/* */) comments
  { 'numToStr/Comment.nvim', opts = {} },

  --  displays a popup with possible key bindings of the command you started typing
  { 'folke/which-key.nvim', opts = {} }, 

  { -- easily jump to any location and enhanced f/t motions for Leap
    'ggandor/leap.nvim',
    config = function()
      require('leap').add_default_mappings(true)
    end,
  },
  { -- clever-f style repeat, with the trigger key itself
    'ggandor/flit.nvim',
    keys = function()
      local ret = {}
      for _, key in ipairs({ 'f', 'F', 't', 'T' }) do
        ret[#ret + 1] = { key, mode = { 'n', 'x', 'o' }, desc = key }
      end
      return ret
    end,
    opts = { labeled_modes = 'nx' },
  },

  { -- Show vertical lines for indent on empty lines
    'lukas-reineke/indent-blankline.nvim',
    opts = function()
      return require 'plugins.configs.indent-blankline'
    end,
  },
  {
    'kyazdani42/nvim-tree.lua',
    dependencies = {
      'kyazdani42/nvim-web-devicons'
    },
    cmd = { "NvimTreeToggle", "NvimTreeFocus" },
    opts = function()
      return require 'plugins.configs.nvimtree'
    end,
  },
  { -- Fuzzy Finder (files, lsp, etc)
    'nvim-telescope/telescope.nvim', 
    version = '*', 
    dependencies = { 
      'nvim-lua/plenary.nvim' 
    },
    cmd = 'Telescope',
    opts = function()
      return require 'plugins.configs.telescope'
    end,
  },
  'ludovicchabant/vim-gutentags', -- tags files management
  'mattn/emmet-vim', -- emmet for vim
  'ap/vim-css-color', -- Preview colours in source code while editing
  'cakebaker/scss-syntax.vim', -- Vim syntax file for scss
  'jwalton512/vim-blade', -- Vim syntax highlighting for Blade templates
  'stephpy/vim-php-cs-fixer', -- automatically fix PHP Coding Standards issues

  { -- Highlight, edit, and navigate code
    'nvim-treesitter/nvim-treesitter',
    dependencies = {
      'nvim-treesitter/nvim-treesitter-textobjects',
    },
    cmd = { "TSInstall", "TSBufEnable", "TSBufDisable", "TSModuleInfo" },
    build = ":TSUpdate",
    opts = function()
      return require 'plugins.configs.treesitter'
    end,
  },

  { -- LSP Configuration & Plugins
    'neovim/nvim-lspconfig', -- Quickstart configurations for the Nvim LSP client 
    dependencies = {
      -- Automatically install LSPs to stdpath for neovim
      'williamboman/mason.nvim', -- Easily install and manage LSP servers, DAP servers, linters, and formatters
      'williamboman/mason-lspconfig.nvim',
      -- Standalone UI for nvim-lsp progress
      { 'j-hui/fidget.nvim',
        tag = "legacy",
        event = "LspAttach",
        opts = {},
      },
    },
  },

  { -- Autocompletion
    'hrsh7th/nvim-cmp',
    dependencies = { 
      'hrsh7th/cmp-nvim-lsp', 
      'L3MON4D3/LuaSnip', 
      'saadparwaiz1/cmp_luasnip' 
    },
    opts = function()
      return require 'plugins.configs.nvim-cmp'
    end,
    -- config = function(_, opts)
    --   require('cmp').setup(opts)
    -- end,
  }
}
