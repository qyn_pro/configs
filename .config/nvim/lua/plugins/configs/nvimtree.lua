--vim.g.nvim_tree_ignore = [ '.git', 'node_modules', '.cache' ]
-- `I` will toggle visibility of folders hidden via |g:nvim_tree_ignore|
-- compact folders that only contain a single folder into one node in the file tree
-- vim.g.nvim_tree_group_empty = 1
-- vim.g.nvim_tree_symlink_arrow = ' >> '
-- will change cwd of nvim-tree to that of new buffer's when opening nvim-tree
-- vim.g.nvim_tree_respect_buf_cwd = 1

vim.api.nvim_create_autocmd({"QuitPre"}, {
    callback = function() vim.cmd("NvimTreeClose") end,
})

return {
  -- https://github.com/nvim-tree/nvim-tree.lua/wiki/Auto-Close
  -- auto_close=true,
  -- updates the root directory of the tree on `DirChanged` (when your run `:cd` usually)
  -- update_cwd=true,
  disable_netrw = true,
  hijack_netrw = true,
  hijack_cursor = true,
  hijack_unnamed_buffer_when_opening = false,
  -- sync_root_with_cwd = true,
  update_focused_file = {
    enable = true,
    update_root = false,
  },
  view = {
    adaptive_size = false,
    side = "left",
    width = 30,
    hide_root_folder = true,
  },
  git = {
    enable = false,
    ignore = true,
  },
  filesystem_watchers = {
    enable = true,
  },
  actions = {
    open_file = {
      resize_window = true,
    },
  },
  renderer = {
    highlight_git = false,
    highlight_opened_files = "none",

    indent_markers = {
      enable = false,
    },

    icons = {
      show = {
        file = true,
        folder = true,
        folder_arrow = true,
        git = false,
      },

      glyphs = {
        default = "",
        symlink = "",
        folder = {
          default = "",
          empty = "",
          empty_open = "",
          open = "",
          symlink = "",
          symlink_open = "",
          arrow_open = "",
          arrow_closed = "",
        },
        git = {
          unstaged = "✗",
          staged = "✓",
          unmerged = "",
          renamed = "➜",
          untracked = "★",
          deleted = "",
          ignored = "◌",
        },
      },
    },
  },
}
