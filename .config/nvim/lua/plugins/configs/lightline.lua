vim.g.lightline = {
  colorscheme = 'nord',
  active = { left = { { 'mode', 'paste' }, { 'readonly', 'filename', 'bufnum', 'modified' } } },
}
