return {
  char = '┊',
  buftype_exclude = { 'terminal', 'nofile' },
  show_trailing_blankline_indent = false,
}
