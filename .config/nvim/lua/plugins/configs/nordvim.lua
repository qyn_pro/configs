vim.g.nord_cursor_line_number_background = 1
vim.g.nord_uniform_diff_background = 1
vim.g.nord_bold_vertical_split_line = 1
vim.g.nord_italic = 1
vim.g.nord_italic_comments = 1
vim.g.nord_underline = 1

-- hi vertsplit ctermfg=bg ctermbg=bg
vim.api.nvim_create_autocmd('ColorScheme', {
  group = vim.api.nvim_create_augroup('nord', { clear = true }),
  callback = function()
    vim.api.nvim_set_hl(0, 'Normal', { fg = NONE })
    vim.api.nvim_set_hl(0, 'LeapLabelPrimary', { fg = '#bf616a' })
    vim.api.nvim_set_hl(0, 'LeapLabelSecondary', { fg = '#5e81ac' })
    vim.api.nvim_set_hl(0, 'TelescopeNormal', { bg = '#3b4252' })
    vim.api.nvim_set_hl(0, 'TelescopeBorder', { fg = '#8fbcbb', bg = '#3b4252' })
    vim.api.nvim_set_hl(0, 'TelescopePromptPrefix', { fg = '#bf616a', bg = '#3b4252' })
  end
})
