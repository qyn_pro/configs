" Highlight on yank
augroup YankHighlight
  autocmd!
  autocmd TextYankPost * silent! lua vim.highlight.on_yank()
augroup END

" When editing a file, always jump to the last known cursor position.
augroup vimStartup
	autocmd!
	autocmd BufReadPost *
		\ if line("'\"") >= 1 && line("'\"") <= line("$") && &ft !~# 'commit'
		\ |   exe "normal! g`\""
		\ | endif
augroup END

augroup FileTypeSpecificAutocommands
  autocmd FileType javascript setlocal expandtab tabstop=4 softtabstop=4 shiftwidth=4
  autocmd FileType php setlocal expandtab tabstop=4 softtabstop=4 shiftwidth=4
  autocmd BufWritePost *.php silent! call PhpCsFixerFixFile()
  autocmd BufRead,BufNewFile *.scss set filetype=scss.css
augroup END

" Resize panes when window/terminal gets resize.
autocmd VimResized * :wincmd =

" Close the loclist window automatically when the buffer is closed.
augroup CloseLoclistWindowGroup
	autocmd!
	autocmd QuitPre * if empty(&buftype) | lclose | endif
augroup END

" disable for buffer:
" autocmd FileType TelescopePrompt lua require('cmp').setup.buffer { enabled = false }
autocmd FileType text lua require('cmp').setup.buffer {
\   sources = {
\     { name = 'path' },
\   },
\ }
