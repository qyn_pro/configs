-- remap `;` as leader key
-- <LocalLeader> is to be used for mappings which are local to a buffer.
-- Make sure to set `mapleader` before lazy so your mappings are correct
vim.keymap.set('', ';', '')
vim.g.mapleader = ';'
vim.g.maplocalleader = ';'

local lazypath = vim.fn.stdpath 'data'  .. '/lazy/lazy.nvim'
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

require('lazy').setup('plugins', {
  change_detection = {
    notify = false,
  }
})

vim.keymap.set({ 'n', 'v' }, '<Space>', '<Nop>', { silent = true })
-- quit! 
vim.keymap.set('n', '<Leader>q', ':xa<CR>', { silent = true })
-- save
vim.keymap.set('n', '<Leader>w', ':w<CR>', { silent = true })
-- remap arrow keys
vim.keymap.set('n', '<Up>', '<C-y>', { silent = true })
vim.keymap.set('n', '<Down>', '<C-e>', { silent = true })
-- switch between tabs
vim.keymap.set('n', '<Right>', 'gt')
vim.keymap.set('n', '<Left>', 'gT')
-- navigate around splits
vim.keymap.set('n', '<C-j>', '<C-w>j', { silent = true })
vim.keymap.set('n', '<C-k>', '<C-w>k', { silent = true })
vim.keymap.set('n', '<C-h>', '<C-w>h', { silent = true })
vim.keymap.set('n', '<C-l>', '<C-w>l', { silent = true })
-- buffer switching
vim.keymap.set('n', '<Leader>b', ':ls<CR>:b ', { silent = true })
-- move between open buffers
vim.keymap.set('n', '<C-n>', ':bnext<CR>', { silent = true })
vim.keymap.set('n', '<C-p>', ':bprev<CR>', { silent = true })
vim.keymap.set('n', '<Leader>d', ':bdelete<CR>', { silent = true })
-- delete a buffer without closing the window
vim.keymap.set('n', '<Leader>bd', ':bprev<CR>:bdelete#<CR>', { silent = true })
-- yank the current filepath
vim.keymap.set('n', '<Leader>cp', ':let @+ = expand("%:p")<CR>', { silent = true })
-- switch CWD to the directory of the open buffer
vim.keymap.set('', '<Leader>cd', ':cd %:p:h<CR>:pwd<CR>')
-- toggle line numbering
vim.keymap.set('n', '<Leader>a', ":exec &nu==&rnu? 'se nu!' : 'se rnu!'<CR>", { silent = true })
-- remap for dealing with word wrap
vim.keymap.set('n', 'k', "v:count == 0 ? 'gk' : 'k'", { expr = true, silent = true })
vim.keymap.set('n', 'j', "v:count == 0 ? 'gj' : 'j'", { expr = true, silent = true })
-- move around in Insert
vim.keymap.set('i', '<C-k>', '<C-o>gk')
vim.keymap.set('i', '<C-h>', '<Left>')
vim.keymap.set('i', '<C-l>', '<Right>')
vim.keymap.set('i', '<C-j>', '<C-o>gj')
-- readline-style mappings for Insert and Command
vim.keymap.set('i', '<C-a>', '<Home>')
vim.keymap.set('i', '<C-e>', '<End>')
vim.keymap.set('c', '<C-a>', '<Home>')
vim.keymap.set('c', '<C-e>', '<End>')
-- U for redo
vim.keymap.set('n', 'U', '<C-r>')
vim.keymap.set('n', 'Y', 'y$')
-- Press * to search for the term under the cursor or a visual selection and
-- then press a key below to replace all instances of it in the current file.
vim.keymap.set('n', '<Leader>r', ":%s///g<Left><Left>")
vim.keymap.set('n', '<Leader>rc', ":%s///gc<Left><Left><Left>")
-- helpful delete/change into blackhole buffer
vim.keymap.set('n', 'x', '"_x', { silent = true })
vim.keymap.set('n', 'dd', '"_dd', { silent = true })
vim.keymap.set('n', 'c', '"_c', { silent = true })
vim.keymap.set('n', 'C', '"_C', { silent = true })
-- opens line below or above the current line
vim.keymap.set('i', '<C-d>', '<C-o>o', { silent = true })
vim.keymap.set('i', '<C-u>', '<C-o>O', { silent = true })

-- Insert mode completion mappings
--   ]     - 'tags' file completion
--   Space - context aware omni completion (via 'omnifunc' setting)
--   d     - dictionary completion (via 'dictionary' setting)
--   f     - file path completion
--   z     - line completion (repeat an existing line)
vim.keymap.set('i', '<C-]>', '<C-x><C-]>')
vim.keymap.set('i', '<C-Space>', '<C-x><C-o>')
vim.keymap.set('i', '<C-d>', '<C-x><C-k>')
vim.keymap.set('i', '<C-f>', '<C-x><C-f>')
vim.keymap.set('i', '<C-z>', '<C-x><C-l>')

-- vim.keymap.set('n', '<C-f>', ':EnableFastPHPFolds<CR>')

-- [[ Nvim Tree keymaps ]]
vim.keymap.set('n', '<Leader>tt', vim.cmd.NvimTreeToggle)
vim.keymap.set('n', '<Leader>tf', vim.cmd.NvimTreeFocus)
vim.keymap.set('n', '<Leader>tr', vim.cmd.NvimTreeRefresh)

-- [[ Telescope keymaps ]]
vim.keymap.set('n', '<Leader>fr', require('telescope.builtin').oldfiles, { desc = '[F]ind [R]ecently opened files' })
vim.keymap.set('n', '<Leader>fb', require('telescope.builtin').buffers, { desc = '[F]ind existing [B]uffers' })
vim.keymap.set('n', '<Leader>/', require('telescope.builtin').current_buffer_fuzzy_find, { desc = '[/] Fuzzily search in current buffer' })

vim.keymap.set('n', '<Leader>ff', require('telescope.builtin').find_files, { desc = '[F]ind [F]iles' })
vim.keymap.set('n', '<Leader>fh', require('telescope.builtin').help_tags, { desc = '[F]ind [H]elp' })
vim.keymap.set('n', '<Leader>fs', require('telescope.builtin').grep_string, { desc = '[F]ind [S]elected word' })
vim.keymap.set('n', '<Leader>fw', require('telescope.builtin').live_grep, { desc = '[F]ind by [G]rep' })
vim.keymap.set('n', '<Leader>fd', require('telescope.builtin').diagnostics, { desc = '[F]nd [D]iagnostics' })

-- Diagnostic keymaps
vim.keymap.set('n', '[d', vim.diagnostic.goto_prev, { desc = "Go to previous diagnostic message" })
vim.keymap.set('n', ']d', vim.diagnostic.goto_next, { desc = "Go to next diagnostic message" })
vim.keymap.set('n', '<Leader>e', vim.diagnostic.open_float, { desc = "Open floating diagnostic message" })
vim.keymap.set('n', '<Leader>l', vim.diagnostic.setloclist, { desc = "Open diagnostics list" })

-- [[ LSP keymaps ]]
--  This function gets run when an LSP connects to a particular buffer.
local on_attach = function(_, bufnr)

  local nmap = function(keys, func, desc)
    if desc then
      desc = 'LSP: ' .. desc
    end

    vim.keymap.set('n', keys, func, { buffer = bufnr, desc = desc })
  end

  -- See `:help vim.lsp.*` for documentation on any of the below functions
  nmap('<Leader>rn', vim.lsp.buf.rename, '[R]e[n]ame')
  nmap('<Leader>ca', vim.lsp.buf.code_action, '[C]ode [A]ction')

  nmap('gd', vim.lsp.buf.definition, '[G]oto [D]efinition')
  nmap('gr', require('telescope.builtin').lsp_references, '[G]oto [R]eferences')
  nmap('gI', vim.lsp.buf.implementation, '[G]oto [I]mplementation')
  nmap('<Leader>D', vim.lsp.buf.type_definition, 'Type [D]efinition')
  nmap('<Leader>ds', require('telescope.builtin').lsp_document_symbols, '[D]ocument [S]ymbols')
  nmap('<Leader>ws', require('telescope.builtin').lsp_dynamic_workspace_symbols, '[W]orkspace [S]ymbols')
  nmap('K', vim.lsp.buf.hover, 'Hover Documentation')
  nmap('<C-k>', vim.lsp.buf.signature_help, 'Signature Documentation')

  -- Lesser used LSP functionality
  nmap('gD', vim.lsp.buf.declaration, '[G]oto [D]eclaration')
  nmap('<Leader>wa', vim.lsp.buf.add_workspace_folder, '[W]orkspace [A]dd Folder')
  nmap('<Leader>wr', vim.lsp.buf.remove_workspace_folder, '[W]orkspace [R]emove Folder')
  nmap('<Leader>wl', function()
    print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
  end, '[W]orkspace [L]ist Folders')

  -- Create a command `:Format` local to the LSP buffer
  vim.api.nvim_buf_create_user_command(bufnr, 'Format', function(_)
    vim.lsp.buf.format()
  end, { desc = 'Format current buffer with LSP' })
end

-- Enable the following language servers
--  Add any additional override configuration in the following tables. They will be passed to
--  the `settings` field of the server config.
local servers = {
  tsserver = {},
  vuels = {},
  intelephense = {},
  cssls = {},
  html = {},
}

-- Enable (broadcasting) snippet capability for completion
local capabilities = vim.lsp.protocol.make_client_capabilities()
-- https://github.com/hrsh7th/vscode-langservers-extracted
capabilities.textDocument.completion.completionItem.snippetSupport = true
capabilities = require('cmp_nvim_lsp').default_capabilities(capabilities)

require('mason').setup()

-- Ensure the servers above are installed
local mason_lspconfig = require 'mason-lspconfig'

mason_lspconfig.setup {
  ensure_installed = vim.tbl_keys(servers),
}

mason_lspconfig.setup_handlers {
  function(server_name)
    require('lspconfig')[server_name].setup {
      capabilities = capabilities,
      on_attach = on_attach,
      settings = servers[server_name],
      -- flags = {
      --   debounce_text_changes = 150,
      -- },
    }
  end,
}

-- `:h option-list` for default values
vim.opt.termguicolors = true
-- save undo information in a file
vim.opt.undofile = true
vim.opt.undodir = os.getenv("XDG_DATA_HOME") .. "/nvim/undo/"
vim.opt.backup = true
vim.opt.backupdir = os.getenv("XDG_DATA_HOME") .. "/nvim/backup/"
vim.opt.swapfile = false
vim.opt.updatetime = 300
-- don't unload buffer when it is abandoned
vim.opt.hidden = true
-- autowrite file if changed
vim.opt.autowrite = true
-- don't redraw while executing macros
vim.opt.lazyredraw = true
-- number of command-lines that are remembered
vim.opt.history = 500
-- time out on mappings
vim.opt.timeout = true
-- time out time for key codes in milliseconds
vim.opt.timeoutlen = 300
-- show @@@ in the last line if it is truncated
vim.opt.display = 'truncate'
-- wrap long lines at a blank
vim.opt.linebreak = true
-- every wrapped line will continue visually indented
vim.opt.breakindent = true
-- string to use at the start of wrapped lines
vim.opt.showbreak = '↳'
-- columns to highlight
vim.opt.colorcolumn = '80'
-- show relative line number in front of each line
vim.opt.relativenumber = true
-- but show the actual number for the line we're on
vim.opt.number = true
-- when and how to display the sign column
vim.opt.signcolumn = 'number'
-- highlight the screen line of the cursor
vim.opt.cursorline = true
-- message on status line to show current mode
vim.opt.showmode = false
-- show whitespace as special chars
vim.opt.list = false
-- splits open bottom right
-- characters for displaying in list mode
vim.opt.listchars = { space = '_', tab = '>~', eol = '¬', nbsp = '+', trail = '-' }
vim.opt.splitbelow = true
vim.opt.splitright = true
-- always display the status line
vim.opt.laststatus = 2
-- number of lines to use for the command-line
vim.opt.cmdheight = 2
vim.opt.shortmess:append { c = true, I = true }
-- adjust case of match for keyword completion
vim.opt.infercase = true
-- no ignore case when pattern has uppercase
vim.opt.smartcase = true
-- ignore case in search patterns
vim.opt.ignorecase = true
-- keep cursor more in middle when scrolling down / up
vim.opt.scrolloff = 999
-- incremental live completion
vim.opt.inccommand = "split"
-- specify how Insert mode completion works
vim.opt.completeopt = { 'menu', 'menuone', 'noselect' }
-- list of file names used for keyword completion
vim.opt.dictionary = { '/usr/share/dict/words' }
-- number of spaces that <Tab> in file uses
vim.opt.tabstop = 2
-- number of spaces that <Tab> uses while editing
vim.opt.softtabstop = 2 
-- number of spaces to use for (auto)indent step
vim.opt.shiftwidth = 2
-- use spaces when <Tab> is inserted
vim.opt.expandtab = true
-- round indent to multiple of 'shiftwidth'
vim.opt.shiftround = true
-- highlight matches with last search pattern
vim.opt.hlsearch = false
-- use one space, not two, after punctuation
vim.opt.joinspaces = false
-- characters to use for displaying special items
--vim.opt.fillchars = { eob = "~" }
vim.opt.fillchars = "fold: "  
vim.wo.foldmethod = "expr"  
vim.opt.foldexpr = "nvim_treesitter#foldexpr()"
vim.opt.foldlevel = 99  

-- syntax to be loaded for current buffer
vim.cmd [[ syntax enable ]]

-- enable the use of mouse clicks
vim.opt.mouse = 'a'

vim.opt.clipboard = { 'unnamed', 'unnamedplus' }

vim.cmd [[ source ~/.config/nvim/autocorrect.vim ]]
vim.cmd [[ source ~/.config/nvim/autocommand.vim ]]
