# Should be called before compinit
zmodload zsh/complist

autoload -Uz compinit && compinit
_comp_options+=(globdots)	# With hidden files.

#setopt MENU_COMPLETE        # Automatically highlight first element of completion menu
setopt AUTO_LIST            # Automatically list choices on ambiguous completion.

# Define completers
zstyle ':completion:*' completer _extensions _complete _approximate

# Colors for files and directory
zstyle ':completion:*:*:*:*:default' list-colors ${(s.:.)LS_COLORS}

# Use cache for commands using cache
zstyle ':completion:*' use-cache on
zstyle ':completion:*' cache-path "${XDG_CACHE_HOME}/zsh/.zcompcache"

# Allow you to select in a menu
zstyle ':completion:*' menu select
# See ZSHCOMPWID "completion matching control"
zstyle ':completion:*' matcher-list '' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*'

# Autocomplete options for cd instead of directory stack
zstyle ':completion:*' complete-options true

zstyle ':completion:*' file-sort modification

# Required for completion to be in good groups (named after the tags)
zstyle ':completion:*' group-name ''
