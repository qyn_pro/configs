#
# ~/.zshenv
#

# environment variables
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CACHE_HOME="$HOME/.cache"

export CARGO_HOME="$XDG_CACHE_HOME/cargo"
export CARGO_HTTP_MULTIPLEXING=false
export npm_config_cache="$XDG_CACHE_HOME/npm"
export npm_config_prefix="$XDG_DATA_HOME/npm-global"
export LESSHISTFILE="-"
export LIBVA_DRIVER_NAME='vdpau'
export VDPAU_DRIVER='nvidia'
# export VAAPI_MPEG4_ENABLED=true
# export LIBVA_DRIVERS_PATH='/usr/lib/dri'
export QT_QPA_PLATFORMTHEME=qt5ct

export EDITOR='nvim'
export PAGER='less'
export TERMINAL='xfce4-terminal'
if [ -n "$DISPLAY" ]; then
    export BROWSER='qutebrowser'
else 
    export BROWSER='elinks'
fi
export SYSTEMD_LESS='FRXMK'

# less environment variables
export LESS=-R
export LESS_TERMCAP_mb=$'\E[1;31m'     # begin blink
export LESS_TERMCAP_md=$'\E[1;36m'     # begin bold
export LESS_TERMCAP_me=$'\E[0m'        # reset bold/blink
export LESS_TERMCAP_so=$'\E[01;44;33m' # begin reverse video
export LESS_TERMCAP_se=$'\E[0m'        # reset reverse video
export LESS_TERMCAP_us=$'\E[1;32m'     # begin underline
export LESS_TERMCAP_ue=$'\E[0m'        # reset underline

# nnn environment variables
# -o	open files only on Enter
# -x	copy path to system clipboard on select
export NNN_OPTS='Hoex'  # binary options to nnn
export NNN_BMS='b:/media/ddata/Books;d:/media/ddata/downloads;'
NNN_BMS+='i:/media/ddata/pics;c:~/Downloads/videos;'
NNN_BMS+='m:/media/ddata/Video/MVid;t:/media/ddata/text;'
NNN_BMS+='v:/media/ddata/Video;p:~/web-apps/dummy-app'
export NNN_PLUG='p:preview-tui;i:imgview;z:fzz;o:fzopen;w:wall'	# key-plugin (or cmd) pairs
export NNN_FCOLORS='6ee64396008b74fda7ad6da7' # file-specific colors
export NNN_FIFO='/tmp/nnn.fifo' #	FIFO to write hovered file path to
#export NNN_OPENER="${XDG_CONFIG_HOME:-$HOME/.config}/nnn/plugins/nuke"
export NNN_TRASH=1	# use trash-cli
export sel=${XDG_CONFIG_HOME:-$HOME/.config}/nnn/.selection
# export USE_PISTOL=1	# use pistol for file previews

# fzf environment variables
export FZF_DEFAULT_COMMAND='fd --type f --hidden --follow --exclude .git'
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"
export FZF_DEFAULT_OPTS="
--exact
--layout=reverse
--info=inline
--height=80%
--multi
--preview-window=:hidden
--preview '([[ -f {} ]] && (bat --style=numbers --color=always {} || cat {})) || ([[ -d {} ]] && (tree -C {} | less)) || echo {} 2> /dev/null | head -200'
--color='fg:#d8dee9,bg:#2e3440,hl:#a3be8c'
--color='fg+:#d8dee9,bg+:#434c5e,hl+:#a3be8c'
--color='info:#4c566a,prompt:#81a1c1,pointer:#bf616a'
--color='marker:#ebcb8b,spinner:#4c566a,header:#4c566a'
--prompt='∼ ' --pointer='▶' --marker='+'
--bind '?:toggle-preview'
--bind 'ctrl-a:select-all,ctrl-d:deselect-all,ctrl-t:toggle'
"
export FZF_CTRL_T_OPTS='--select-1 --exit-0'
export FZF_ALT_C_OPTS="--select-1 --exit-0 --preview 'tree -C {} | head -200'" 

# clipmenud collects and caches what's on the clipboard
export CM_DEBUG=0
export CM_SELECTIONS="clipboard"
export CM_MAX_CLIPS=20
export CM_LAUNCHER=fzf
export CM_OUTPUT_CLIP=1
